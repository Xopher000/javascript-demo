let person = {
  _name: 'Kris',
  _age: 22,
  weekendAlarm: 'No alarms needed',
  weekAlarm: 'Alarm set to 7AM',
  
  sayHello: function() {
    return `Hello, my name is ${this._name}`;
  },
  
  sayGoodbye() {
    return 'Goodbye!';
  }
  
  set age(newAge){
    if(typeof newAge === 'number'){
      this._age = newAge
    } else {
      return 'Invalid input';
    }
  }
  
  get age() {
    console.log(`${this._name} is ${this._age} years old.`);
    return this._age;
  }
  
};

let friend = {
  _name: 'CJ'
}

friend.sayHello = person.sayHello;

console.log(person.sayHello());

person.hobbies = ['Video Games', 'Traveling'];
person.hobbies = ['Video Games'];
console.log(person.hobbies);


console.log(person['_name']);
console.log(person['_age']);

let day = 'Friday';
let alarm;

if (day === 'Saturday' || day === 'Sunday' ) {
  alarm = 'weekendAlarm';
} else {
  alarm = 'weekAlarm';
}

console.log(person[alarm]);

console.log(friend.sayHello());

person.age = 'Thirty-nine';

person.age = 39;

console.log(person.age);